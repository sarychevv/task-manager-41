package ru.t1.sarychevv.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserRepository {

    @Nullable
    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {@Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")})
    UserDTO findOneByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {@Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")})
    UserDTO findOneByEmail(@NotNull @Param("email") String email);

    @Update("UPDATE tm_user SET login = #{login}, password = #{passwordHash}, email = #{email}, " +
            "locked = #{locked}, first_name = #{firstName}, last_name = #{lastName}, " +
            "middle_name = #{middleName}, role = #{role} WHERE id = #{id}")
    void update(@NotNull UserDTO user);

    @Insert("INSERT INTO tm_user (id, login, password, email, locked, " +
            "first_name, last_name, middle_name, role)" +
            " VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{locked}, " +
            "#{firstName}, #{lastName}, #{middleName}, #{role})")
    UserDTO add(@NotNull UserDTO model);

    @NotNull
    @Select("SELECT * FROM tm_user")
    @Results(value = {@Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")})
    List<UserDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {@Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")})
    UserDTO findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_user WHERE LIMIT 1 OFFSET #{index}")
    @Results(value = {@Result(property = "passwordHash", column = "password"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    UserDTO findOneByIndex(@NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_user")
    void removeAll();

    @Nullable
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    UserDTO removeOne(@NotNull UserDTO model);

    @Nullable
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    UserDTO removeOneById(@NotNull @Param("id") String id);

    @Nullable
    @Delete("DELETE FROM tm_user WHERE id = (SELECT id FROM tm_user WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    UserDTO removeOneByIndex(@NotNull @Param("index") Integer index);

    @NotNull
    @Select("SELECT COUNT(*) FROM tm_user")
    Integer getSize();

}
