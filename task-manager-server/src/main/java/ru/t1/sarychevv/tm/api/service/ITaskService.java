package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.enumerated.Status;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable
    TaskDTO create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    TaskDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    TaskDTO updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable
    TaskDTO changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @Nullable
    TaskDTO changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId) throws Exception;

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable Comparator<TaskDTO> comparator) throws Exception;

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    TaskDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    TaskDTO removeOne(@Nullable String userId, @Nullable TaskDTO model) throws Exception;

    @Nullable
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    TaskDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO model) throws Exception;
}
