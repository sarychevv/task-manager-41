package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionService {

    @Nullable
    SessionDTO add(@Nullable String userId, @Nullable SessionDTO model) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    List<SessionDTO> findAll(@Nullable String userId) throws Exception;

    @Nullable
    SessionDTO findOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    SessionDTO findOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @Nullable
    SessionDTO removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    SessionDTO removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    @Nullable
    SessionDTO removeOne(@Nullable SessionDTO model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;
}
