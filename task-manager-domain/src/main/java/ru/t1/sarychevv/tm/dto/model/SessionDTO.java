package ru.t1.sarychevv.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
public final class SessionDTO extends AbstractUserOwnedModelDTO {

    @Column
    @NotNull
    private Date date = new Date();

    @Column
    @Enumerated(EnumType.STRING)
    @Nullable
    private Role role = null;

    @Column
    @NotNull
    private String name = "";

    @Column
    @NotNull
    private String description = "";
}

